# deadc0de ASM linker

Creates IPS file from ASM, using "DE AD C0 DE  <3-byte memory location>" embedded syntax in the ASM.

Based on linker of same name by Lemmyz

There are certainly better ways to do this, but it works.

