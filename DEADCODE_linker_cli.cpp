
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <bitset>

const std::string MAGICWORD = "DEADC0DE";
const std::string ipsdata_PATCH = "5041544348"; //"PATCH"
const std::string ipsdata_EOF = "454F46"; //"EOF"

std::string ToHex(const std::string& s, bool upper_case)
{
    std::ostringstream ss;

    for (std::string::size_type i = 0; i < s.length(); ++i)
    {
        int z = s[i] & 0xff;
        ss << std::hex << std::setfill('0') << std::setw(2) << (upper_case ? std::uppercase : std::nouppercase) << z;
    }

    return ss.str();
}
std::string ToHex(int i, bool upper_case)
{
    std::ostringstream ss;

    ss << std::hex << std::setfill('0') << std::setw(2) << (upper_case ? std::uppercase : std::nouppercase) << i;
    
    return ss.str();
}


int main(int argc, char* argv[])
{
    
    if (argc != 3) {

        std::cout << "Requires input and output files.\n" << std::endl;
        std::cout << "USAGE:" << std::endl;
        std::cout << "  cmd <input_file.bin> <output_file.ips>\n" << std::endl;

        exit(0);
    }


    //std::string infile = "a.o65";
    //std::string outfile = "ips.ips";
    std::string infile = argv[1];
    std::string outfile = argv[2];

    // actual output, initialize the string with IPS header
    std::string ipsdata = ipsdata_PATCH;

    std::ifstream::pos_type size;
    char* memblock;
    std::string filehex;

    
    // open file
    std::ifstream inputfile(infile, std::ios::binary);

    if (inputfile.is_open())
    {
        inputfile.seekg(0, std::ios::end);
        size = inputfile.tellg();
        memblock = new char[size];
        inputfile.seekg(0, std::ios::beg);
        inputfile.read(memblock, size);
        inputfile.close();

        // Convert file to Hex
        filehex = ToHex(std::string(memblock, size), true);
        std::cout << "File read." << std::endl;

    }
    else
    {
        std::cout << "Error could not open source file." << std::endl;
        exit(0);
    }

    int index = 0;
    bool continueloop = true;
    std::string codesegment;

    while (continueloop)
    {
        // get one character
        std::string hexchar = filehex.substr(index, 1);
        // get next 8 characters to see if matchines MAGICWORD
        std::string hexmagicword = filehex.substr(index, MAGICWORD.length());
        
        if (hexmagicword.compare(MAGICWORD) == 0)
        {
            // get memory location from 3 bytes after MAGICWORD
            std::string memlocation = filehex.substr(index + MAGICWORD.length(), 6);
            
            // get length of code up to this point
            int codelen = codesegment.length() / 2;
            std::string codelength = ToHex(codelen, true);
            codelength = std::string(4 - std::min(4, (int) codelength.length()), '0') + codelength;
            
            // add to IPS file
            ipsdata.append(memlocation);
            ipsdata.append(codelength);
            ipsdata.append(codesegment);

            // update variables for next section
            index += 14; // increment index
            codesegment = "";

        }
        else
        {
            codesegment += hexchar;
            ++index;
        }

        // end if EOF
        if (index >= filehex.length())
            continueloop = false;
        
    }

    // end IPS file
    ipsdata.append(ipsdata_EOF);

    
    // write hex string out to actual IPS file
    std::basic_string<uint8_t> bytes;
    for (size_t i = 0; i < ipsdata.length(); i += 2)
    {
        uint16_t byte;
        std::string nextbyte = ipsdata.substr(i, 2);
        std::istringstream(nextbyte) >> std::hex >> byte;
        bytes.push_back(static_cast<uint8_t>(byte));
    }
    std::string result(begin(bytes), end(bytes));

    // open output file
    std::ofstream outputfile(outfile, std::ios::binary | std::ios::out);
    if (outputfile.is_open())
    {
        outputfile << result;
        outputfile.close();

        std::cout << "IPS file created." << std::endl;
    }
    else
    {
        std::cout << "Error could not create file." << std::endl;
        exit(0);
    }
    
}

