#include <QCoreApplication>
#include <QTextStream>
#include <QFile>
#include <QByteArray>
#include <QDataStream>
#include <QCryptographicHash>

int main(int argc, char *argv[])
{
    //-----------------------------------------
    //magic word used for the deadcode linker's end-of-code magic word
    //-----------------------------------------
     QString sMAGICWORD = "DEADC0DE";
    //-----------------------------------------
    //variables for this project
    //-----------------------------------------
    //const int iMaxFileData = 1000;
    //QString sFileData[iMaxFileData];
    //QString sFileName_Source[iMaxFileData];
    QString sFileData;
    QString sFileName_Source; //source

    //-----------------------------------------
    //start sIPSFILEDATA IPS file format
    //-----------------------------------------
    QString sIPSFILEDATA_PATCH = "5041544348"; //"PATCH"
    QString sIPSFILEDATA_EOF = "454F46"; //"EOF"
    //-------------------------------------------------------
    //Use data from table_patcher to generate the IPS
    //-------------------------------------------------------
    //qstring for data to write to ips file
    QString sIPSFILEDATA = sIPSFILEDATA_PATCH;


    //-----------------------------------------
    int exe_return_value = 0;
    //-----------------------------------------
    QCoreApplication a(argc, argv);
    ////-----------------------------------------
    ////variables
    ////-----------------------------------------
    QString sAllArguments ;
    bool ok;    //used for qt qstring functions
    ////-----------------------------------------
    ////get count of arguements
    ////-----------------------------------------
    int count_of_arguements = a.arguments().count();
    ////-----------------------------------------
    ////get all arguements(except the exename, index0) as QString
    ////-----------------------------------------
    int arguement_index = 1; //0 == program exe name
    while (arguement_index < count_of_arguements)
    {
        //get next arguement command
        QString sCURRENT_ARGUEMENT = a.arguments().at(arguement_index);
        //append
        if (arguement_index > 1) sAllArguments.append(" ");
        sAllArguments.append(sCURRENT_ARGUEMENT);
        //increment
        arguement_index++;
    }    //end while
    //-----------------------------------------
    //split
    //-----------------------------------------
    QStringList listArguments = sAllArguments.split(' ');
    //-----------------------------------------
    //parse: get arguement for source filename
    //-----------------------------------------
     QString sARGUMENT_source = listArguments.at(0);
    //-----------------------------------------
    //parse: help
    //-----------------------------------------
    if (sARGUMENT_source.isNull()) { sARGUMENT_source = "help"; }
    if (sARGUMENT_source.right(4).compare("help") == 0) {
        //QTextStream(stdout) <<  << endl;
        QTextStream(stdout) << "DEADC0DE linker, build Mar20_2017" << endl;

        QTextStream(stdout) << "USAGE:" << endl;
        QTextStream(stdout) << "DEADC0DE_linker_cli (source file) (target rom file)" << endl;
        QTextStream(stdout) << "DEADC0DE_linker_cli (source file) (target .ips file)" << endl;
        QTextStream(stdout) << "EXAMPLE:" << endl;
        QTextStream(stdout) << "DEADC0DE_linker_cli a.o65 rom_file.nes" << endl;


        return 1;
    }

    //-----------------------------------------
    //variables
    //-----------------------------------------
    bool BURN_OPERATION_ROM_FILE = false;   //includes .ips patcher automatically
    //-----------------------------------------
    //parse: get arguement for target filename
    //-----------------------------------------
    QString sARGUMENT_target = listArguments.at(1);
    //check if .ips file
    if (!sARGUMENT_target.isEmpty())
    {
             BURN_OPERATION_ROM_FILE =true;
    }


    //-----------------------------------------
    //parse: commands
    //-----------------------------------------
    //-------------------------------------------------
    //load
    //-------------------------------------------------
        //-------------------------------------------------
        //load
        //-------------------------------------------------
        if (!sARGUMENT_source.isEmpty())
        {
            sFileName_Source=sARGUMENT_source;
            //display status
            //QTextStream(stdout) << "load, filename: " << sFileName_Source << endl;
            QFile file(sFileName_Source);
            //open file
            if (!file.open(QIODevice::ReadOnly))
            {
                QTextStream(stdout) << "load ERROR: file did not open: " << sFileName_Source << endl;
                return 1 ; //safety check
            }
            //load as hex
             sFileData = file.readAll().toHex().toUpper();
            //close
            file.close();
        }
        sARGUMENT_source=""; //clear so no other arguments are detected

    //-------------------------------------------------
    //get info
    //-------------------------------------------------
    QString sBinaryData;
    QString sBurnLocation;
    int StringIndex=0;
    int iMagicWordSize=sMAGICWORD.size() ;
    bool bContinueLoop = true;
    while (bContinueLoop)
    {
        //get next
        QString sCHAR=sFileData.mid(StringIndex,2);
        if (!sCHAR.isEmpty())
        {
        //append
        sBinaryData.append(sCHAR);
        //check for magic word "DEADC0DE"
        QString sMAGICWORD_CHECK=sFileData.mid(StringIndex+2,iMagicWordSize);
        //QTextStream(stdout) << sMAGICWORD_CHECK << endl;
        //compare
        if (sMAGICWORD_CHECK.compare(sMAGICWORD) == 0)
        {
            //get burn location
            sBurnLocation=sFileData.mid(StringIndex+2+iMagicWordSize,6);
            //show info
            QTextStream(stdout) << "ADDR=";
            QTextStream(stdout) << sBurnLocation ;
            QTextStream(stdout) << " DATA=";
            QTextStream(stdout) << sBinaryData << endl;
            //do burn operation
            if (BURN_OPERATION_ROM_FILE)
            {
                QString sSRC_data_asHex = sBinaryData ;
                QString sDST_addy = sBurnLocation ;
                int iDST_addy = sDST_addy.toInt(&ok,16); iDST_addy = iDST_addy * 2;
                int iLength_for_HexText = sBinaryData.size() ; //string form
                //QString sLENGTH = QString::setNum(iLength_for_HexText,16);
                QString sLENGTH ;
                sLENGTH .setNum(iLength_for_HexText/2,16);
                //--------------------
                //pick filename
                QString sTargetFileName=sARGUMENT_target;
                //open target file
                QFile Ifile(sTargetFileName);
                Ifile.open(QIODevice::ReadOnly);
                //read data in
                QString sTARGETDATA_asHex = Ifile.readAll().toHex().toUpper();
                //close file
                Ifile.close();
                //--------------------
                //variables
                QString sNew_TARGETDATA_asHex="";
                //go through all index and check if at dest , then copy custom data, otherwise copy original
                for (int i=0; i < sTARGETDATA_asHex.size() ;i++)
                {
                    sNew_TARGETDATA_asHex.append(sTARGETDATA_asHex.at(i));
                    if (i==iDST_addy-1)
                    {
                        sNew_TARGETDATA_asHex.append(sSRC_data_asHex);
                        i=i+iLength_for_HexText;
                    }
                } //end for
                //--------------------
                //open target file
                QFile Ofile(sTargetFileName);
                Ofile.open(QIODevice::WriteOnly);
                QByteArray binarydata = QByteArray::fromHex(sNew_TARGETDATA_asHex.toLatin1());
                Ofile.write(binarydata);
                Ofile.close();
                //--------------------

                //--------------------
                //build sIPSFILEDATA
                //--------------------
                sIPSFILEDATA.append(sDST_addy.rightJustified(6, '0'));
                sIPSFILEDATA.append(sLENGTH.rightJustified(4, '0'));
                sIPSFILEDATA.append(sSRC_data_asHex);

            } //end if (BURN_OPERATION_ROM_FILE)

            //reset
            sBinaryData.clear();
            sBurnLocation.clear();
            //increment
            StringIndex=StringIndex+iMagicWordSize+6;
        } //end compare

        //increment
        StringIndex+=2;
        }
        //check for end
        if (sCHAR.isEmpty())
            bContinueLoop = false;
    }   //end while
    //-------------------------------------------------
    //show info
    //-------------------------------------------------
    //QTextStream(stdout) << sBinaryData << endl;

    //-------------------------------------------------
    //IPS
    //-------------------------------------------------
    //write sIPSFILEDATA "EOF"
    sIPSFILEDATA.append(sIPSFILEDATA_EOF);
    //-------------------------------------------------
    //pick target file name
    QString sFileName=sARGUMENT_target;
    sFileName.append(".ips");
    //open target file
    QFile Ofile(sFileName);
    //open file
    Ofile.open(QIODevice::WriteOnly);
    //QDataStream out(&Ofile);
    QByteArray binarydata = QByteArray::fromHex(sIPSFILEDATA.toLatin1());
    //write binary data
    Ofile.write(binarydata);
    //close file
    Ofile.close();
    //-------------------------------------------------


    //-------------------------------------------------
    //md5sum
    //-------------------------------------------------
    // Returns empty QByteArray() on failure.
        QFile f(sARGUMENT_target);
        if (f.open(QFile::ReadOnly)) {
            //load as hex
             QByteArray bData = f.readAll();//.toHex();
                QString blah = QString(QCryptographicHash::hash(bData,QCryptographicHash::Md5).toHex());
                QTextStream(stdout) << blah << " *" << sARGUMENT_target << endl;
            //close file
                f.close();

            }




    return exe_return_value;
}
